import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  feature: string = 'recipe';

  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyDrDAwp_6ruGrx7z80QVVWrdCdbR1uA1no",
      authDomain: "ng-recipe-app-f6d8c.firebaseapp.com",
      databaseURL: "https://ng-recipe-app-f6d8c.firebaseio.com",
      projectId: "ng-recipe-app-f6d8c",
    });
  }

  onNavigate(feature: string) {
    this.feature = feature
  }
}
