import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HeaderComponent } from "./header/header.component";
import { HomeComponent } from "./home/home.component";
import { AppRoutingModule } from "../app-routing.module";
import { SharedModule } from "../shared/shared-module.module";
import { ShoppingListService } from "../shopping-list/shopping-list.service";
import { RecipeService } from "../recipes/recipe.service";
import { DataStoreService } from "../shared/data-store.service";
import { AuthService } from "../auth/auth.service";

@NgModule({
    declarations: [
        HeaderComponent,
        HomeComponent
    ],
    imports: [CommonModule, AppRoutingModule, SharedModule],
    exports: [AppRoutingModule, HeaderComponent],
    providers: [ShoppingListService, RecipeService, DataStoreService, AuthService]
})
export class CoreModule {

}